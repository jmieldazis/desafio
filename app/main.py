from models.compute_skills_payload import ComputeSkillsPayload
from fastapi import FastAPI


app = FastAPI()


@app.post("/compute_skills/")
async def compute_skills(compute_skills_payload: ComputeSkillsPayload):
    freelancer = compute_skills_payload.freelance

    return {
        "freelance": {
            "id": freelancer.id,
            "computedSkills": freelancer.computed_skills(),
        }
    }
