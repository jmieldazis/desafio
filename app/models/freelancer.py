from datetime import datetime
from typing import Dict, List, Optional
from pydantic import BaseModel

from .professional_experience import ProfessionalExperience
from .user import User
from utils import (
    get_all_days_experience,
    remove_overlapped_days,
    get_num_days,
    get_num_months,
)


class Freelancer(BaseModel):
    id: int
    user: User
    status: str
    retribution: int
    availabilityDate: datetime
    professionalExperiences: Optional[List[ProfessionalExperience]] = []

    def __repr__(self):
        return self.full_name

    @property
    def full_name(self) -> str:
        return self.user.full_name

    @property
    def skills(self) -> List:
        skills = []
        for professional_experience in self.professionalExperiences:
            skills += professional_experience.skills

        return list(set(skills))

    def get_date_ranges_by_skill(self) -> Dict[str, List]:
        result = {}
        for professional_experience in self.professionalExperiences:
            for skill in professional_experience.skills:
                try:  # add a date range to an existing skill in the results list
                    result[skill.name].append(
                        {
                            "start_date": professional_experience.startDate,
                            "end_date": professional_experience.endDate,
                        }
                    )
                except KeyError:  # add a new skill with its respective date range
                    result[skill.name] = [
                        {
                            "start_date": professional_experience.startDate,
                            "end_date": professional_experience.endDate,
                        }
                    ]

        return result

    def get_duration_in_months_by_skill(self) -> Dict[str, int]:
        skills = {}
        for skill, date_ranges in self.get_date_ranges_by_skill().items():
            all_days_experience = get_all_days_experience(date_ranges)
            days = remove_overlapped_days(all_days_experience)
            num_days = get_num_days(days)
            num_months = get_num_months(num_days)

            skills[skill] = num_months

        return skills

    def computed_skills(self):
        duration_in_months_by_skill = self.get_duration_in_months_by_skill()

        return [
            {
                "id": skill.id,
                "name": skill.name,
                "durationInMonths": duration_in_months_by_skill.get(skill.name),
            }
            for skill in self.skills
        ]
