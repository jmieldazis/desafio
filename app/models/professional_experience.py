from datetime import datetime
from typing import List, Optional
from pydantic import BaseModel

from .skill import Skill


class ProfessionalExperience(BaseModel):
    id: int
    companyName: str
    startDate: datetime
    endDate: datetime
    skills: Optional[List[Skill]] = []

    def __repr__(self):
        return f"{self.companyName} | {self.startDate} - {self.endDate} | {self.skills}"
