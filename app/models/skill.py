from pydantic import BaseModel


class Skill(BaseModel):
    id: int
    name: str

    def __hash__(self):
        return hash(("id", self.id, "name", self.name))

    def __eq__(self, other):
        return self.id == other.id and self.name == other.name

    def __repr__(self):
        return f"{self.name}"
