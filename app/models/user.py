from pydantic import BaseModel


class User(BaseModel):
    firstName: str
    lastName: str
    jobTitle: str

    def __repr__(self):
        return f"{self.firstName} {self.lastName} | {self.jobTitle}"

    @property
    def full_name(self):
        return f"{self.firstName} {self.lastName}"
