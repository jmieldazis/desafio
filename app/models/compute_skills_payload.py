from pydantic import BaseModel
from .freelancer import Freelancer


class ComputeSkillsPayload(BaseModel):
    freelance: Freelancer
