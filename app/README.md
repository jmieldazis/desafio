# Orama

## Getting started

### Make
After creating and activating the python virtual environment, you can:
```shell
$ make install
```
To run the project:
```shell
$ make run
```
To run the project with docker:
```shell
$ make run_docker
```
To run tests:
```shell
$ make test
```
___
### Docker
```shell
$ docker build -t orama -f docker/Dockerfile .
$ docker run -p 80:80 orama
```
----
### Run in your host
If you don't want to use the docker, you can run the project on your machine.
#### Create virtual environment
```shell
$ python3 -m venv env
$ source env/bin/activate
```
#### Install dependencies
Run this command in the same folder where is `requirements.txt`
```shell
$ pip install -r requirements.txt
```
#### Run project
```shell
$ uvicorn main:app
```
___
### API Documentation
To access the api documentation you must, after running the project, access http://localhost/docs or http://localhost/redoc.

___

### Run tests
In the main folder of the project, you can run:
```shell
$ python -m unittest
```
___
### Common errors:

##### Host Permission
> ...starting container process caused "exec: \"/app/docker/entrypoint.sh\": permission denied": unknown

This error is caused because the `user is not allowed to execute the "entrypoint.sh"` file. If you use a system other than linux, search `how to change file execution permission`. If you use linux, you can resolve it by running this command in your shell:
```shell
$ chmod +x entrypoint.sh
```
