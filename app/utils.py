from datetime import datetime, date
from typing import List, Dict


NUM_DAYS_IN_MONTH = 30


def format_date(date_as_str: str) -> datetime:
    return datetime.strptime(date_as_str[0:10], "%Y-%m-%d")


def get_all_days_experience(date_ranges: List[Dict[str, datetime]]) -> List[str]:
    days = []
    for date_range in date_ranges:
        all_days_between_date_range = get_all_days_between_date_range(**date_range)
        days += all_days_between_date_range

    return days


def get_all_days_between_date_range(
    start_date: datetime, end_date: datetime
) -> List[str]:
    dates = [
        date.fromordinal(date_).isoformat()
        for date_ in range(start_date.toordinal(), end_date.toordinal() + 1)
    ]

    return dates


def remove_overlapped_days(days: List[str]) -> List[str]:
    return list(set(days))


def get_num_days(days: List[str]) -> int:
    return len(days)


def get_num_months(num_days: int) -> int:
    return num_days // NUM_DAYS_IN_MONTH if num_days > 0 else 0
