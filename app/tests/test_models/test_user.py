import unittest

from models.user import User


class TestUser(unittest.TestCase):
    def setUp(self):
        atributes = {
            "firstName": "Hunter",
            "lastName": "Moore",
            "jobTitle": "Fullstack JS Developer",
        }
        self.user = User(**atributes)

    def test_user_is_User(self):
        self.assertIsInstance(self.user, User)

    def test_atributes(self):
        self.assertEqual(self.user.firstName, "Hunter")
        self.assertEqual(self.user.lastName, "Moore")
        self.assertEqual(self.user.jobTitle, "Fullstack JS Developer")

    def test_full_name(self):
        self.assertEqual(self.user.full_name, "Hunter Moore")

    def test_repr(self):
        self.assertTrue(self.user.firstName in repr(self.user))
        self.assertTrue(self.user.lastName in repr(self.user))
        self.assertTrue(self.user.jobTitle in repr(self.user))
