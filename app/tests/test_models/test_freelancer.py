import datetime
import unittest

from models.freelancer import Freelancer
from models.skill import Skill
from models.professional_experience import ProfessionalExperience
from models.user import User


class TestFreelancer(unittest.TestCase):
    def setUp(self):
        self.skill = Skill(id=1, name="CSS")
        self.skill_2 = Skill(id=2, name="Javascript")
        self.user = User(firstName="Teste", lastName="Silva", jobTitle="JS developer")
        self.professional_experience = ProfessionalExperience(
            id=1, 
            companyName="Teste", 
            startDate=datetime.datetime(2020, 1, 1), 
            endDate=datetime.datetime(2020, 12, 1),
            skills=[self.skill]
        )
        self.professional_experience_2 = ProfessionalExperience(
            id=2, 
            companyName="Orama", 
            startDate=datetime.datetime(2020, 6, 1), 
            endDate=datetime.datetime(2020, 10, 1),
            skills=[self.skill, self.skill_2]
        )
        atributes = {
            "id": 1,
            "user": self.user,
            "status": "new",
            "retribution": 500,
            "availabilityDate": datetime.datetime(2020, 12, 1),
            "professionalExperiences": [self.professional_experience, self.professional_experience_2]
        }
        self.freelancer = Freelancer(**atributes)

    def test_freelancer_is_Freelancer(self):
        self.assertIsInstance(self.freelancer, Freelancer)

    def test_atributes(self):
        self.assertEqual(self.freelancer.id, 1)
        self.assertEqual(self.freelancer.status, "new")
        self.assertEqual(self.freelancer.availabilityDate, datetime.datetime(2020, 12, 1))
        self.assertEqual(self.freelancer.professionalExperiences[0].id, 1)

    def test_has_user(self):
        self.assertEqual(self.freelancer.user.firstName, "Teste")

    def test_full_name(self):
        self.assertEqual(self.freelancer.full_name, self.freelancer.user.full_name)

    def test_skills(self):
    	self.assertTrue(self.skill in self.freelancer.skills)
    	self.assertTrue(self.skill_2 in self.freelancer.skills)
    	self.assertEqual(len(self.freelancer.skills), 2)

    def test_get_date_ranges_by_skill(self):
    	date_ranges_by_skill = self.freelancer.get_date_ranges_by_skill()
    	self.assertEqual(len(date_ranges_by_skill.get("CSS")), 2)
    	self.assertEqual(len(date_ranges_by_skill.get("Javascript")), 1)
    	self.assertEqual(date_ranges_by_skill.get("Javascript")[0].get("start_date"), datetime.datetime(2020, 6, 1))
    	self.assertEqual(date_ranges_by_skill.get("Javascript")[0].get("end_date"), datetime.datetime(2020, 10, 1))

    def test_get_duration_in_months_by_skill(self):
    	duration_in_months_by_skill = self.freelancer.get_duration_in_months_by_skill()
    	self.assertEqual(duration_in_months_by_skill.get("CSS"), 11)
    	self.assertEqual(duration_in_months_by_skill.get("Javascript"), 4)

    def test_computed_skills(self):
    	computed_skills = self.freelancer.computed_skills()
    	self.assertEqual(len(computed_skills), 2)
    	self.assertTrue({ 'id': 1, 'name': 'CSS', 'durationInMonths': 11 } in computed_skills)

    def test_skill_not_repeat(self):
        self.freelancer.professionalExperiences[0].skills.append(self.skill)
        self.assertEqual(len(self.freelancer.skills), 2)

    def test_repr(self):
        self.assertTrue(self.freelancer.full_name in repr(self.freelancer))
