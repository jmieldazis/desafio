import unittest

from models.skill import Skill


class TestSkill(unittest.TestCase):
    def setUp(self):
        atributes = {"id": 1, "name": "CSS"}
        self.skill = Skill(**atributes)

    def test_skill_is_Skill(self):
        self.assertIsInstance(self.skill, Skill)

    def test_atributes(self):
        self.assertEqual(self.skill.id, 1)
        self.assertEqual(self.skill.name, "CSS")

    def test_repr(self):
        self.assertTrue(self.skill.name in repr(self.skill))

    def test_hash(self):
        skill1 = Skill(id=1, name="teste")
        skill2 = Skill(id=1, name="teste")
        self.assertTrue(hash(skill1) == hash(skill2))

    def test_eq(self):
        skill1 = Skill(id=1, name="teste")
        skill2 = Skill(id=1, name="teste")
        self.assertTrue(skill1 == skill2)

        skill1 = Skill(id=1, name="teste")
        skill2 = Skill(id=2, name="teste")
        self.assertFalse(skill1 == skill2)
