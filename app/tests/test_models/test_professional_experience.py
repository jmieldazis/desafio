import datetime
import unittest

from models.skill import Skill
from models.professional_experience import ProfessionalExperience


class TestProfessionalExperience(unittest.TestCase):
    def setUp(self):
        atributes = {
            "id": 1,
            "companyName": "Teste",
            "startDate": datetime.datetime(2020, 1, 1),
            "endDate": datetime.datetime(2020, 12, 1),
            "skills": [Skill(id=1, name="test_skill")]
        }
        self.professional_experience = ProfessionalExperience(**atributes)

    def test_professional_experience_is_ProfessionalExperience(self):
        self.assertIsInstance(self.professional_experience, ProfessionalExperience)

    def test_atributes(self):
        self.assertEqual(self.professional_experience.companyName, "Teste")
        self.assertEqual(self.professional_experience.startDate, datetime.datetime(2020, 1, 1))
        self.assertEqual(self.professional_experience.endDate, datetime.datetime(2020, 12, 1))
        self.assertEqual(self.professional_experience.skills[0].id, 1)

    def test_has_skill(self):
        self.assertEqual(1, len(self.professional_experience.skills))

    def test_repr(self):
        self.assertTrue(self.professional_experience.companyName in repr(self.professional_experience))
