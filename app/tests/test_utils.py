import unittest
import datetime

from utils import (
    format_date,
    get_num_days,
    get_num_months,
    remove_overlapped_days,
    get_all_days_between_date_range,
    get_all_days_experience,
)


class TestUtils(unittest.TestCase):
    def setUp(self):
        self.date_1 = format_date("2020-10-10 00:00")
        self.date_2 = format_date("2021-10-10")

    def test_format_date_return_datetime(self):
        self.assertIsInstance(self.date_1, datetime.datetime)

    def test_format_date_ok(self):
        self.assertEqual(self.date_1.year, 2020)
        self.assertEqual(self.date_1.month, 10)
        self.assertEqual(self.date_1.day, 10)

    def test_get_num_days(self):
        self.assertEqual(1, get_num_days(["2020-01-01"]))
        self.assertEqual(0, get_num_days([]))
        self.assertEqual(
            5,
            get_num_days(
                ["2020-01-01", "2020-01-04", "2020-01-02", "2020-02-12", "2020-05-22"]
            ),
        )

    def test_get_num_months(self):
        self.assertEqual(0, get_num_months(0))
        self.assertEqual(0, get_num_months(1))
        self.assertEqual(0, get_num_months(3))
        self.assertEqual(1, get_num_months(30))
        self.assertEqual(1, get_num_months(31))

    def test_remove_overlapped_days(self):
        days_without_overlapped = remove_overlapped_days(
            ["2020-01-01", "2020-02-01", "2020-01-01", "2020-02-01", "2020-01-01"]
        )
        days_without_overlapped.sort()
        self.assertEqual(["2020-01-01", "2020-02-01"], days_without_overlapped)

    def test_get_all_days_between_date_range(self):
        self.assertEqual(
            ["2020-01-01", "2020-01-02", "2020-01-03", "2020-01-04", "2020-01-05"],
            get_all_days_between_date_range(
                start_date=datetime.datetime(2020, 1, 1),
                end_date=datetime.datetime(2020, 1, 5),
            ),
        )
        self.assertEqual(
            ["2020-01-01"],
            get_all_days_between_date_range(
                start_date=datetime.datetime(2020, 1, 1),
                end_date=datetime.datetime(2020, 1, 1),
            ),
        )
        self.assertEqual(
            [],
            get_all_days_between_date_range(
                start_date=datetime.datetime(2020, 1, 2),
                end_date=datetime.datetime(2020, 1, 1),
            ),
        )

    def test_get_all_days_experience(self):
        self.assertEqual([], get_all_days_experience([]))
        self.assertEqual(
            ["2020-01-01", "2020-01-02", "2020-01-03"],
            get_all_days_experience(
                [
                    {
                        "start_date": datetime.datetime(2020, 1, 1),
                        "end_date": datetime.datetime(2020, 1, 3),
                    }
                ]
            ),
        )
        self.assertEqual(
            [
                "2020-01-01",
                "2020-01-02",
                "2020-01-03",
                "2020-02-10",
                "2020-02-11",
                "2020-02-12",
                "2020-02-13",
                "2020-02-14",
                "2020-02-15",
            ],
            get_all_days_experience(
                [
                    {
                        "start_date": datetime.datetime(2020, 1, 1),
                        "end_date": datetime.datetime(2020, 1, 3),
                    },
                    {
                        "start_date": datetime.datetime(2020, 2, 10),
                        "end_date": datetime.datetime(2020, 2, 15),
                    },
                ]
            ),
        )
