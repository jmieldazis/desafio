import unittest

from main import app

from fastapi.testclient import TestClient


class TestMain(unittest.TestCase):
	def setUp(self):
		self.client = TestClient(app)

	def _format_computed_skills(self, computed_skills):
		return {
			computed_skill.get("name"): computed_skill.get("durationInMonths") 
			for computed_skill in computed_skills
		}

	def test_compute_skills_with_many_experiences_and_overlap(self):
		json = {
		    "freelance": {
		        "id": 42,
		        "user": {
		            "firstName": "Hunter",
		            "lastName": "Moore",
		            "jobTitle": "Fullstack JS Developer",
		        },
		        "status": "new",
		        "retribution": 650,
		        "availabilityDate": "2018-06-13T00:00:00+01:00",
		        "professionalExperiences": [
		            {
		                "id": 4,
		                "companyName": "Okuneva, Kerluke and Strosin",
		                "startDate": "2016-01-01T00:00:00+01:00",
		                "endDate": "2018-05-01T00:00:00+01:00",
		                "skills": [
		                    {"id": 241, "name": "React"},
		                    {"id": 270, "name": "Node.js"},
		                    {"id": 370, "name": "Javascript"},
		                ],
		            },
		            {
		                "id": 54,
		                "companyName": "Hayes - Veum",
		                "startDate": "2014-01-01T00:00:00+01:00",
		                "endDate": "2016-09-01T00:00:00+01:00",
		                "skills": [
		                    {"id": 470, "name": "MySQL"},
		                    {"id": 400, "name": "Java"},
		                    {"id": 370, "name": "Javascript"},
		                ],
		            },
		            {
		                "id": 80,
		                "companyName": "Harber, Kirlin and Thompson",
		                "startDate": "2013-05-01T00:00:00+01:00",
		                "endDate": "2014-07-01T00:00:00+01:00",
		                "skills": [
		                    {"id": 370, "name": "Javascript"},
		                    {"id": 400, "name": "Java"},
		                ],
		            },
		        ],
		    }
		}

		response = self.client.post("/compute_skills/", json=json)
		self.assertEqual(response.json().get("freelance").get("id"), 42)
		self.assertEqual(len(response.json().get("freelance").get("computedSkills")), 5)
		self.assertEqual(response.status_code, 200)

		compute_skills_formated = self._format_computed_skills(response.json().get("freelance").get("computedSkills"))
		self.assertEqual(compute_skills_formated.get("Java"), 40)
		self.assertEqual(compute_skills_formated.get("MySQL"), 32)
		self.assertEqual(compute_skills_formated.get("Node.js"), 28)
		self.assertEqual(compute_skills_formated.get("React"), 28)
		self.assertEqual(compute_skills_formated.get("Javascript"), 60)

	def test_compute_skills_without_experiences(self):
		json = {
		    "freelance": {
		        "id": 60,
		        "user": {
		            "firstName": "Hunter",
		            "lastName": "Moore",
		            "jobTitle": "Fullstack JS Developer",
		        },
		        "status": "new",
		        "retribution": 650,
		        "availabilityDate": "2018-06-13T00:00:00+01:00",
		        "professionalExperiences": [],
		    }
		}

		response = self.client.post("/compute_skills/", json=json)
		self.assertEqual(response.json().get("freelance").get("id"), 60)
		self.assertEqual(len(response.json().get("freelance").get("computedSkills")), 0)
		self.assertEqual(response.status_code, 200)

		compute_skills_formated = self._format_computed_skills(response.json().get("freelance").get("computedSkills"))
		self.assertEqual(compute_skills_formated, {})

	def test_compute_skills_with_one_experience(self):
		json = {
		    "freelance": {
		        "id": 70,
		        "user": {
		            "firstName": "Hunter",
		            "lastName": "Moore",
		            "jobTitle": "Fullstack JS Developer",
		        },
		        "status": "new",
		        "retribution": 650,
		        "availabilityDate": "2018-06-13T00:00:00+01:00",
		        "professionalExperiences": [
		        	{
		                "id": 4,
		                "companyName": "Okuneva, Kerluke and Strosin",
		                "startDate": "2016-01-01T00:00:00+01:00",
		                "endDate": "2018-05-01T00:00:00+01:00",
		                "skills": [
		                    {"id": 241, "name": "React"},
		                    {"id": 270, "name": "Node.js"},
		                    {"id": 370, "name": "Javascript"},
		                ],
		            }
		        ],
		    }
		}

		response = self.client.post("/compute_skills/", json=json)
		self.assertEqual(response.json().get("freelance").get("id"), 70)
		self.assertEqual(len(response.json().get("freelance").get("computedSkills")), 3)
		self.assertEqual(response.status_code, 200)

		compute_skills_formated = self._format_computed_skills(response.json().get("freelance").get("computedSkills"))
		self.assertEqual(compute_skills_formated.get("Javascript"), 28)
		self.assertEqual(compute_skills_formated.get("React"), 28)
		self.assertEqual(compute_skills_formated.get("Node.js"), 28)

	def test_compute_skills_with_one_skill(self):
		json = {
		    "freelance": {
		        "id": 80,
		        "user": {
		            "firstName": "Hunter",
		            "lastName": "Moore",
		            "jobTitle": "Fullstack JS Developer",
		        },
		        "status": "new",
		        "retribution": 650,
		        "availabilityDate": "2018-06-13T00:00:00+01:00",
		        "professionalExperiences": [
		        	{
		                "id": 4,
		                "companyName": "Okuneva, Kerluke and Strosin",
		                "startDate": "2016-01-01T00:00:00+01:00",
		                "endDate": "2018-05-01T00:00:00+01:00",
		                "skills": [
		                    {"id": 370, "name": "Javascript"},
		                ],
		            },
		            {
		                "id": 54,
		                "companyName": "Hayes - Veum",
		                "startDate": "2014-01-01T00:00:00+01:00",
		                "endDate": "2016-09-01T00:00:00+01:00",
		                "skills": [
		                    {"id": 370, "name": "Javascript"},
		                ],
		            },
		            {
		                "id": 80,
		                "companyName": "Harber, Kirlin and Thompson",
		                "startDate": "2013-05-01T00:00:00+01:00",
		                "endDate": "2014-07-01T00:00:00+01:00",
		                "skills": [
		                    {"id": 370, "name": "Javascript"},
		                ],
		            },
		        ],
		    }
		}

		response = self.client.post("/compute_skills/", json=json)
		self.assertEqual(response.json().get("freelance").get("id"), 80)
		self.assertEqual(len(response.json().get("freelance").get("computedSkills")), 1)
		self.assertEqual(response.status_code, 200)

		compute_skills_formated = self._format_computed_skills(response.json().get("freelance").get("computedSkills"))
		self.assertEqual(compute_skills_formated.get("Javascript"), 60)

	def test_invalid_payload(self):
		json = {
		    "freelance": {}
		}

		response = self.client.post("/compute_skills/", json=json)
		self.assertEqual(response.status_code, 422)
